from django.shortcuts import render

from goods.models import Categories

def index(request):
    
    context = {
        'Title': 'Главная',
        'content': 'Магазин мебели HOME',
    }

    return render(request, 'main/index.html', context)

def about(request):
    context = {
        'Title': 'Home - О нас',
        'content': 'О нас',
        'text_on_page': 'Типа вся информация о компании которой не существует и вообще похуй на нее всем ведь это просто курс'
    }

    return render(request, 'main/about.html', context)